import java.util.Random;
import java.util.Scanner;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;


public class Main {
    public static void main(String[] args) throws IOException {
//        System.out.println("Hello world!");
//
//        Урок 4. Цикл do..while, конструкция switch..case
//                Цель задания:
//        Знакомство с циклом do while, более глубокое понимание
//        работы циклов, формирование понимания различия циклов  do while и цикла while
//        Задание:
//        1.
//        С помощью цикла do..while, выведите римские цифры (код начинается с
//                8544). Начало: char c = 8544; ...

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!");
       char c = 8544;
        do {
            System.out.println(c);
            c++;
        } while  (c<8556);

//        2.
//        С помощью цикла do..while, выведите числа от
//                -
//                0.9 до 0.9
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!");
        double dd=0.9;
        do {
            System.out.println(dd);
            dd++;
        } while  (dd<0.9);




//        3.
//        Пользователь вводит строку, выведите каждый второй символ (подсказка:
//        str.charAt(i) )

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner = new Scanner(System.in);
        String str3= scanner.nextLine();
        int i3=0;
        do {
            if (i3!=0&&i3%2==0) System.out.println(str3.charAt(i3));
            i3++;
        } while  (i3<str3.length());



//        4.
//        Пользователь
//        вводит строку. Выводите все слова на разных строках
//        (подсказка : if (str.charAt(i)==‘ ‘) System.out.println(); )

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!");
        String str4= scanner.nextLine();
        int i4=0;
        do {
            System.out.print(str4.charAt(i4));
            if (str4.charAt(i4)==' ') {
                System.out.println(" ");
            }
            i4++;
        } while  (i4<str4.length());



//        5.
//        Пользователь вводит строку. Выведите первые 3 слова

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!");
        String str5 = scanner.nextLine();
        int i5 = 0, i5_1=1;
        do {
            System.out.print(str5.charAt(i5));
            if (str5.charAt(i5) == ' ') {
                if (i5_1<3){
                System.out.println(" ");
                i5_1++;}
                else
                    break;
            }
            i5++;
        } while  (i5<str5.length());
//        6.
//        Пользователь вводит строку, выведите ее задом
//        -
//                наперёд.


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!");
        String str6 = scanner.nextLine();
        for (int i=str6.length();i>0;i--)
        {
            System.out.print(str6.charAt(i-1));
        }

//        7.
//        Пользователь вводит букву. Выведите
//        любое слово, начинающуюся на эту
//        букву

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!");
        String wordString = " абрикос автор авторучка автобус адрес аист акула алмаз алфавит ананас " +
                "бабочка бабушка багаж бадминтон базар баклажан баккара балкон банан бандит " +
                "вагон вадит важный вакцина валенок ваниль ванна вапор вариант варка " +
                "гавань гагара гадалка гадание гадюка гайка галерея галоп галстук галчонок " +
                "давка давление дагер дайвинг дакота далекий дама данные дарить дата " +
                "евангелие евро егерь едок единица единорог ежевика ежик ежиха езда " +
                "ёжик ёлка ёрник ёрш ёршиться ёрзать ёкать ёкнуть ёкарный ёкарь " +
                "жабо жабра жадина жадность жажда жакет жалоба жанр жара жардин " +
                "забава забег забор забота завал заварка завеса загадка загар загляденье " +
                "ибис ива иваси ивняк игла иглобрюх игра игрок идея иена " +
                "йога йогурт йодль йоркшир йорк йот йота йул йули йумор " +
                "кабак кабан кабачок кабель кабина кавалер кавказ кавардак кавычка кадет " +
                "лавка лавочка лагерь ладан ладья лазер лазурь лайка лакмус лампа " +
                "мавзолей магазин магия магнит мадам майонез макака макет мальчик манеж " +
                "набег наблюдать навар нагар награда нагрев надгробие наезд нажим назавтра " +
                "овал овод овощ овраг овца огонь огород огурец одежда одеяло " +
                "павлин пагода падение пазл пайка пакет палата палец палитра палка " +
                "рабочий равнина радио радость радуга район райская ракета рама рандеву " +
                "сабля савана саванна савок саговник сагуаро садик садовник сажа сазан " +
                "табак табачок таблица табун табурет тавот тавро таган тагил тайга " +
                "убежище убивать убор убрать уважать уважение увалень увал увекист увекичь " +
                "фавор фаворит фаворитка фаготрубач фагот фаза файл факел факир фактор " +
                "хабар хайку хакасия хала халат халва халиф халстрон хамелеон хамса " +
                "цапля цапфа царапина цардам царевич цари цвекалить цвел цветение цветок " +
                "чабан чабер чабрец чавкать чаво чадо чадра чайка чайный чалма " +
                "шабаш шабер шабрение шавка шагал шаг шадр шажок шайба шайка " +
                "щавель щебень щебет щебетать щегол щеголь щедрый щека щелок щель " +
                "ъяз ъюл ъыкц ъёдор ъэнерго ъяхта ъыричный ъянкинск ъювеж ъяство " +
                "ывать ывка ыгал ыкать ылить ымкать ындать ындекс ырать ыркать " +
                "эбен эбонит эвен эведи эвенк эвент эвита эволюция эгида эгине " +
                "юбка юбочка ювелир югыд юдоль юкагир юкола юкон юла юлить " +
                "ябеда ябедник явление явить ягода ягуар ядро язва язык якать ";

        Scanner scanner = new Scanner(System.in);
        String str7 = scanner.next();
        char ch7 = str7.charAt(0);

        //в ch7 у нас первый символ от введённой строки. считается что пользователь вводит лишь один символ.

        String ss=(" "+ch7);
        Random random = new Random();

        //в i7 получаем случайное число из 7, так как у нас в строке данных всех слов, по 7 слов для каждой буквы алфавита.

        int i7=random.nextInt(7);
        System.out.println("i7="+i7);

        int Start = wordString.indexOf(ss);;

        System.out.println("Start="+Start);
        for (int i=0;i<i7;i++) {
        Start = wordString.indexOf(ss);
        }

        Integer ii7=wordString.indexOf((ss), Start+1);

        System.out.println("ii7="+ii7);
        String main7 = wordString.substring(ii7,wordString.indexOf((" "), ii7+1));
        System.out.println("main7 = "+main7);







//        8.
//        Пользователь вводит цифру: выведите страну, телефонный код которого
//        начинается на эту цифру






        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!");
//        String page = downloadWebPage("https://en.wikipedia.org/wiki/List_of_mobile_telephone_prefixes_by_country");
//        System.out.println(page);

       Scanner scanner8 = new Scanner(System.in);
        System.out.println("Введите цифру:");
        String cod=scanner8.next();


        //мы знаем что в файле cod_country.txt  103 строки
        for(int i=1;i<=103;i++){
            String str8=findFirstCh(FileS.getStringFromFile("cod_country.txt",i),cod.charAt(0));
           if (str8!="")
            System.out.println(str8);
//            System.out.println(FileS.getStringFromFile("cod_country.txt",i));
        }

//        9.
//        Пользователь вводит цифру. Если она от 0 до 6
//                -
//                пусть вводит еще.
//            Если 9
//                -
//                конец. Если 7,8
//                -
//                пусть вводит строки до тех пор,
//                пока не введет «стоп» (на
//        этом все).

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 9 !!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner9 = new Scanner(System.in);
        System.out.println("введите число от 0 до 9");
        int i9=scanner9.nextInt();
        String str9="";
        boolean EXIT=false;
        while (!EXIT)
            switch (i9){
            case 0,1,2,3,4,5,6:

                System.out.println("вы ввели "+i9+" поэтому вводите ещё пока не введёте больше числа 6:");
                                i9=scanner9.nextInt();
                                break;
            case 9: EXIT=true;
                break;
            case 7,8: while (str9.indexOf("стоп")<0){
                if (!str9.equals(""))
                    System.out.println("вы ввели ("+str9+") поэтому вводите строки, пока не введёте слово <стоп>");
                else System.out.println("введите строку");
                str9=scanner9.nextLine();
            }
            EXIT=true;
            break;
            }
        System.out.println("Конец");



//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для ввода
//        -
//                вывода данных
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно
//        более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов

    }

    private static String downloadWebPage(String url) throws IOException{
        StringBuilder result = new StringBuilder();
        String line;
        URLConnection urlConnection = new URL(url).openConnection();
        try (InputStream is = urlConnection.getInputStream();
             BufferedReader br= new BufferedReader(new InputStreamReader(is))){
            while ((line=br.readLine())!=null){
                result.append(line);
            }
        }
        return result.toString();



    }
    ////        //функция поиска  'первого числа' в строке с кодом

        public static String findFirstCh(String str,char ch){
                if (str.charAt(1)==ch)
                    return str;
                else return "";
        }


}